"use client";
import Image from "next/image";
import styles from "../page.module.scss";
import Header from "../components/header";
import Footer from "../components/footer";
import Card from "../components/card";

import React, { useEffect } from "react";

export default function Home() {
  const cardContent1 = [
    "Dịch vụ khám chữa bệnh",
    "Dịch vụ xét nghiệm, chuẩn đoán hình ảnh",
    "Dịch vụ chăm sóc, điều trị",
    "Dịch vụ tại nhà",
  ];
  const cardContent2 = [
    "Danh mục bác sĩ",
    "CV bác sĩ",
    "Content 3",
    "Content 4",
  ];
  const cardContent3 = ["Content 1", "Content 2", "Content 3", "Content 4"];
  const cardContent4 = ["Content 1", "Content 2", "Content 3", "Content 4"];
  return (
    <>
      <Header />
      <div className={styles.main}>
        <Image
          className={styles.logo}
          src="/logotaodan.png"
          alt="Next.js Logo"
          width={300}
          height={80}
          priority
        />
        <section className={styles.gridgt}>
          <div className={styles.gthieu}>Giới thiệu</div>
          <div className={styles.ndgthieu}>
            Phòng khám Đa khoa Tao Đàn nằm trong số những Phòng khám uy tín ở Hồ
            Chí Minh. Với đội ngũ tay nghề cao và chất lượng phục vụ tốt, Phòng
            khám Đa khoa Tao Đàn từ lâu là địa chỉ Phòng khám tin cậy cho mọi
            người ở khu vực Quận 1 Hồ Chí Minh. Phòng khám được trang bị các
            thiết bị cơ sở y tế và cơ sở vật chất hiện đại cùng với không gian
            thoáng mát, sạch sẽ phục vụ cho việc khám chữa bệnh đạt kết quả
            chính xác và nhanh nhất.
            <br />
            <br />
            Thời gian làm việc:
            <br />
            Từ thứ 2 tới thứ 7: 7g - 19g
            <br />
            Chủ Nhật: 7g - 11g
            <br />
            <br />
            Thông tin liên hệ:
            <br />
            Địa chỉ: 75 - 77 Thạch Thị Thanh, Phường Tân Định, Quận 1, Hồ Chí
            Minh
            <br />
            Điện thoại: 0283 8207546
          </div>
        </section>
      </div>
      <Footer />
    </>
  );
}
